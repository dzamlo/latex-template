# Latex template

To build the document, you should use `latexmk -pdf`. 

This template use the `minted` package. This package need the `pygmentize` command.
